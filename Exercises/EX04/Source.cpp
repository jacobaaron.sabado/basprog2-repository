#include<iostream>
#include<string>
#include<time.h>

using namespace std;

struct Item
{
	string name;
	int gold;
};
Item* generateItem() {
	Item* items;
	Item* item = new Item;
	int randomizer = rand() % 5;
	if (randomizer == 0) {
		item->name = "Cursed Stone";
		item->gold = 0;

		items = item; //Adds item to array
	}
	if (randomizer == 1) {
		item->name = "Jellopy";
		item->gold = 5;

		items = item;
	}
	if (randomizer == 2) {
		item->name = "Thick Leather";
		item->gold = 25;

		items = item;
	}
	if (randomizer == 3) {
		item->name = "Sharp Talon";
		item->gold = 50;

		items = item;
	}
	if (randomizer == 4) {
		item->name = "Mithril Ore";
		item->gold = 100;

		items = item;
	}
	Item* finalItem = item;

	return finalItem;
}
void enterDungeon(int& totalMoney, int& multiplier, int& dungeonMoney){
	cout << "You have " << totalMoney << " gold in your pocket right now.\n" << endl;
	totalMoney -= 25;
	cout << "You pay the fee. You now have " << totalMoney << " gold. Happy travels.\n" << endl;
	system("pause");
	   Item* finalItem = generateItem();
	   if ((*finalItem).name == "Cursed Stone") {
		   cout << "You got a Cursed Stone! You just died and lost all your gold. Sorry about that." << endl;
		   dungeonMoney = 0;
		   multiplier = 1;
		   return;
	   }
	   else {
		   cout << "You got " << finalItem->name << "! You earned " << finalItem->gold * multiplier << " gold!" << endl;
		   dungeonMoney += finalItem->gold * multiplier;
	   }
		cout << "You have " << dungeonMoney << " in your backpack." << endl;
		system("pause");
		string yesNo;
		cout << "Do you want to continue looting? Input Yes or No." << endl;
		cin >> yesNo;
		if (yesNo == "Yes") {
			totalMoney += 25; //Cancels out the -25 fee 
			multiplier++;
			cout << "A magical force has multiplied the value of the items by " << multiplier << "!" << endl;
			return;
		}
		else if (yesNo == "No") {
			totalMoney += dungeonMoney;
			cout << "You now have " << totalMoney << endl;
			dungeonMoney = 0;
			multiplier = 1;
		}
		delete finalItem;
}
int main(){
	srand(time(NULL));
	Item items[5];
	int totalMoney = 50;
	int multiplier = 1;
	int dungeonMoney = 0;

	cout << "-----------------------------\nWelcome to the Dungeon.\n-----------------------------\n\nHere you must keep looting until you get 500 gold, or you run out.\n\nThe fee is 25 gold per expedition\n\n Good Luck." << endl;
	system("pause");
	system("cls");
	while (totalMoney >= 25 && totalMoney < 500) {

		enterDungeon(totalMoney, multiplier, dungeonMoney);	
		system("pause");
		system("cls");
	}
	if (totalMoney <= 25) {
		cout << "Looks like you ran out of cash. Better luck next time.\n" << endl;
	}
	else {
		cout << "Congrats. You just earned enough gold. Now go buy something nice.\n" << endl;
	}
	return 0;
}
