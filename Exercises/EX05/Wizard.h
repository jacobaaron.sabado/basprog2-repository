#pragma once
#include<string>
using namespace std;

class Spell;

class Wizard
{
private:
	//Data Members
	string mName;
	int mHp;
	int mMp;
	int mMinDamage;
	int mMaxDamage;

public:
	//Method
	void setName(string setter);	
	void attack();
};

