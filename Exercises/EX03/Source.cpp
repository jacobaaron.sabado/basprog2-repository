#include<iostream>
#include<string>
#include<time.h>

using namespace std;

//Ex 3-1 Function
void arrayPointer(int* numbers) {

	for (int i = 0; i < 20; i++) {
		numbers[i] = rand() % 101;
		cout << numbers[i] << endl;
		
	}
}

//Ex 3-2
int arrayFiller(int* numbers1, int n) {	
	cout << "Input array size: " << endl;
	cin >> n;

	for (int i = 0; i < n; i++) {
		numbers1[n] = rand() % 101;
		cout << numbers1[n] << endl;
	}

	return *numbers1;
}

//Ex 3-3
void deallocator(int* numbers2, int n) {
	
	cout << "Input array size: " << endl;
	cin >> n;

	for (int i = 0; i < n; i++) {
		numbers2[n] = rand() % 101;
	}
	
	delete[] numbers2;
}

int main() {

	srand(time(0));

	int n = 0;
	int* numbers0 = new int[20];
	arrayPointer(numbers0);

	int* numbers1 = new int[n];
	arrayFiller(numbers1, n);

	int* numbers2 = new int[n];
	deallocator(numbers2, n);
	cout << numbers2 << endl;

	return 0;
}

