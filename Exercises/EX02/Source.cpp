#include <iostream>
#include <time.h>
#include <string>
#include <vector>

using namespace std;

void itemFiller(vector<string>& items, int& randomizer) {
	//Randomizer will randomly assign each vector
	for (int a = 0; a < 10; a++) {
		randomizer = rand() % 4 + 1;	
	if (randomizer == 1){
		items.push_back("RedPotion");
	}
	if (randomizer == 2) {
		items.push_back("Elixir");
	}
	if (randomizer == 3) {
		items.push_back("EmptyBottle");
	}
	if (randomizer == 4) {	
		items.push_back("BluePotion");
	}
		
	}
}

void itemPrinter(const vector<string>& items) {
	//Prints Vector
	for (int i = 0; i < items.size(); i++) {
		cout << i + 1 << ".) " << items[i] << endl;
	}

}

void itemCounter(vector<string>& items) {
	string rpCounter = "RedPotion", eCounter = "Elixir", ebCounter = "EmptyBottle", bpCounter = "BluePotion";
	int rpCount = 0, eCount = 0, ebCount = 0, bpCount = 0;
	for (int i = 0; i < items.size(); i++) {
		//If item is equal to the string, add 1 to the counter
		if (items[i] == rpCounter) {
			rpCount++;
		}
		if (items[i] == eCounter) {
			eCount++;
		}
		if (items[i] == ebCounter) {
			ebCount++;
		}
		if (items[i] == bpCounter) {
			bpCount++;
		}
	}
	//Prints the Final Count
	cout << "Red Potions: " << rpCount << endl;
	cout << "Elixirs: " << eCount << endl;
	cout << "Empty Bottles: " << ebCount << endl;
	cout << "BLue Potions: " << bpCount << endl;
}

void itemRemover(vector<string>& items, int& remover) {
	//Asks user to select number to remove
	cout << "Select an item to remove. Refer to the number order." << endl;
	cin >> remover;
	cout << "Removing item " << remover << "..." << endl;
	system("pause");

	//Just in case there's a wrong output
	if (remover > items.size() || remover <= 0) {

		cout << "Invalid number, no items have been removed." << endl;
		system("pause");
		return;

	}

	items.erase(items.begin() + remover - 1);
}

int main() {

	srand(time(NULL));

	int randomizer, remover;
	vector<string> items;

	itemFiller(items, randomizer);
	itemPrinter(items);
	itemCounter(items);
	itemRemover(items, remover);

	//Prints final Inventory
	cout << "Final inventory: \n"  << endl;
	itemPrinter(items);
}