#include <iostream>
#include <string>
#include<time.h>

using namespace std;

struct Node
{
	std::string name;
	Node* next = NULL;
	Node* previous = NULL; //this is optional
};

void createList(Node*& head){ //To actually create the list
	string soldierName[5];

	for (int i = 0; i < 5; i++) {
		string namer;
		cout << "What is your name soldier? ";
		cin >> namer;
		soldierName[i] = namer;
 	}

	Node* current = nullptr;
	Node* previous = nullptr;

	//Creates 1st Node
	current = new Node;
	current->name = soldierName[0];	
	head = current; //Set the head node to the first node
	previous = current;

	//Creates 2nd Node
	current = new Node;
	current->name = soldierName[1];
	previous->next = current;
	previous = current; //CONTINUES THE LINKED LIST

	//Creates 3rd Node
	current = new Node;
	current->name = soldierName[2];
	previous->next = current;
	previous = current;

	//Creates 4th Node
	current = new Node;
	current->name = soldierName[3];
	previous->next = current;
	previous = current;

	//Create 5th and Last Node
	current = new Node;
	current->name = soldierName[4];
	previous->next = current;
	current->next = head; //END OF THE CIRCULAR LINKED LIST
}
void printList(Node* head) { //To print the list

	//Prints the Linked List
	Node* counter = head; // Think of this like int i = 0

		do {
			cout << counter->name << endl; //Prints list
			counter = counter->next; //Basically i++ from a for function
		} while (counter != head);


}
void changeHead(Node*& head, int size) { //To change list every round
	int indexToChange = rand() % size + 1;
	Node* temp = head; //Holds the key to transform into head node
	for (int i = 0; i < indexToChange; i++){ //Traverses nodes until it reaches the node to be changed
		temp = temp->next;
	}
	head = temp->next; //Changes node to head node
	cout << endl << head->name << " holds the cloak." << endl;
}
void deleteFirst(Node*& head) //Just in case a head node needs to be deleted
{
	Node* nextNode = head; //variable for the node to turn into a head
    Node* firstNode = head; //variable for the head node to be deleted

	//Traverses immediately to the next node
	while (nextNode->next != head) {
		nextNode = nextNode->next;
	}
	nextNode->next = firstNode->next; //Link next node to first node	
	head = nextNode->next; // Makes second node as head node 
	cout << firstNode->name << " has been eliminated.\n\n" << endl;
	//Deletes the former head node
	delete firstNode;
	firstNode = nullptr;
}
void deleteNode(Node*& head, int size) { //To delete a node
	int indexToRemove = rand() % size + 1;
	cout << "The number chosen is " << indexToRemove << endl;
	Node* toDelete = head;
	Node* previous1 = nullptr;
	//Traverses the nodes
	for (int i = 0; i < indexToRemove; i++) {
		//Keeps track of the previous node
		previous1 = toDelete;
		//Moves pointer to the next node
		toDelete = toDelete->next;
	}
	if (indexToRemove == size) { //If the index goes back to the head, in this case equal to the size, use this function to remove head
		deleteFirst(head);
	}
	else { //Else just link the nodes as usual and delete accordingly.
		previous1->next = toDelete->next;

		cout << toDelete->name << " has been eliminated.\n\n" << endl;
		//Deletes Node
		delete toDelete;
		toDelete = nullptr;
	}
}
void playRound(Node*& head, int& size){ //To run the entire game
	int roundCount = 1;
	createList(head);
	system("cls");

	while (size != 1) {
		cout << "ROUND " << roundCount++ << endl << endl;
		printList(head);
		changeHead(head, size);
		deleteNode(head, size);
		size--;
		system("pause");
		system("cls");
    }
	cout << head->name << " will seek for reinforcements. \nGodspeed to the rest of you." << endl;
}

int main(){
	srand(time(NULL));
	Node* head = nullptr;
	int size = 5; //Size of the Soldiers
	playRound(head, size);

	return 0;
}