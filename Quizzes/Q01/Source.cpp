#include<iostream>
#include<string>
#include<time.h>
#include<vector>

using namespace std;

void createCards(vector<string>& kaijiCards, vector<string>& tonegawaCards, int round) {
	vector<string> emperorCards;
	emperorCards.push_back("Emperor");
	for (int i = 0; i < 4; i++) {
		emperorCards.push_back("Citizen");
	}
	vector<string> slaveCards;
	slaveCards.push_back("Slave");
	for (int i = 0; i < 4; i++) {
		slaveCards.push_back("Citizen");
	}
	if (round == 1 || round == 2 || round == 3 || round == 7 || round == 8 || round == 9) {
		kaijiCards = emperorCards;
		tonegawaCards = slaveCards;
	}
	else {
		kaijiCards = slaveCards;
		tonegawaCards = emperorCards;
	}
}
void wager(int& wagerMm, int& round, int& mmLeft) {
	//While wager is 0 or less or more than mmLeft, keep asking for wager
	while (wagerMm > mmLeft || wagerMm <= 0) {
		cout << "How much will you wager, Kaiji?" << endl;
		cin >> wagerMm;
	}
}
string kaijiTurn(vector<string>& kaijiCards) {
	int chooser;
	string kaijiSelected;
	for (int i = 0; i < kaijiCards.size(); i++) {
		cout << "[" << i + 1 << "] " << kaijiCards[i] << endl;
	}
	cout << "\nWhat card will you play? (refer to the number)" << endl;
	cin >> chooser;
	kaijiSelected = kaijiCards[chooser - 1];
	kaijiCards.erase(kaijiCards.begin() + chooser - 1); //deletes card after initialized
	return kaijiSelected;
}
string tonegawaTurn(vector<string>& tonegawaCards) {
	int randomizer = rand() % tonegawaCards.size();
	string tonegawaSelected;
	tonegawaSelected = tonegawaCards[randomizer];
	tonegawaCards.erase(tonegawaCards.begin() + randomizer);
	return tonegawaSelected;
}
void finalPayout (string kaijisCard, string tonegawasCard, int wagerMm, int& moneyEarned, int& mmLeft){
    if (kaijisCard == "Emperor" && tonegawasCard == "Citizen" || kaijisCard == "Citizen" && tonegawasCard == "Slave") {
		cout << "Congrats. You win " << wagerMm * 100000 << " Yen." << endl;
		moneyEarned += wagerMm * 100000; //Reward for Emperor Side
	}
	else if (kaijisCard == "Slave" && tonegawasCard == "Emperor") {
		cout << "Impressive! You win 5x your amount. A total of " << wagerMm * 100000 * 5 << " Yen." << endl;
		moneyEarned += wagerMm * 100000 * 5; //Reward for Slave Side
	}
	else {
		cout << "You Lose. The drill will now move " << wagerMm << " milimeters." << endl;
		mmLeft -= wagerMm; //Punishment for losing
	}
}
void playRound(int round, int& moneyEarned, int& mmLeft) {
	vector<string> kaijiCards;
	vector<string> tonegawaCards;
	createCards(kaijiCards, tonegawaCards, round);
	int wagerMm;
	cout << "Round " << round << "/12" << endl << "Money: " << moneyEarned << endl << "Milimeters left: " << mmLeft << endl << "Side: " << kaijiCards[0] << endl;
	wager(wagerMm, round, mmLeft); //Function for wagering
	system("cls");
	string kaijisCard = kaijiTurn(kaijiCards); //Chosen card for Player
	string tonegawasCard = tonegawaTurn(tonegawaCards); // Chosen card for AI
	system("cls");
	while (kaijisCard == "Citizen" && tonegawasCard == "Citizen") {
		cout << "You played " << kaijisCard << "\nTonegawa played " << tonegawasCard << "\n\n" << endl;
		cout << "A draw" << endl;
		system("pause");
		system("cls");
		kaijisCard = kaijiTurn(kaijiCards); //Chosen card for Player
		tonegawasCard = tonegawaTurn(tonegawaCards); // Chosen card for AI
		system("cls");
	}
	cout << "You played " << kaijisCard << "\nTonegawa played " << tonegawasCard << "\n\n" << endl;
	finalPayout(kaijisCard, tonegawasCard, wagerMm, moneyEarned, mmLeft);
	system("pause");
	system("cls");
}
int main() {
	srand(time(NULL));

	int round = 1;
	int	mmLeft = 30;
	int moneyEarned = 0;

	while (mmLeft != 0 && round <= 12) {
		playRound(round, moneyEarned, mmLeft);

		round++;
	}
	if (mmLeft == 0){
		cout << "The drill has pierced your eardrum. \nIt doesn't matter what you won. \nYou cannot claim what has been lost." << endl;
	}
	else if (moneyEarned >= 20000000) {
		cout << "You won 20,000,000 Yen. \nTonegawa bows at your feet \nin utter shame and defeat." << endl;
	}
	else {
		cout << "While you didn't lose your ear, \nyou didn't earn 20,000,000 Yen either. \nThe souls of your friends who passed will haunt you forever." << endl;
	}
	cout << endl;

	return 0;
}
