#include "Crystal.h"

Crystal::Crystal(string name, int gainCrystals)
	: Item(name)
{
	mGainCrystals = gainCrystals;
}

void Crystal::activate(Unit* target)
{
	target->earnCrystal(mGainCrystals);
	cout << "You gained " << mGainCrystals << " Crystals!" << endl;
}
