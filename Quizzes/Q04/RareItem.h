#pragma once
#include<iostream>
#include<string>
#include "Item.h"
using namespace std;
class RareItem :
    public Item
{
public:

    RareItem(string name, int rarityValue);

    void activate(Unit* target);

private:
    int mRarityValue;
};

