#pragma once
#include<string>
#include<iostream>
#include "Item.h"
using namespace std;
class HealPotion :
    public Item
{
public:

    HealPotion(string name, int healValue);

    void activate(Unit* target);

private:
    int mHealValue;
};

