#include "Unit.h"

Unit::Unit()
{
    mHp = 100;
    mRarityPoints = 0;
    mCrystals = 100;
}

Unit::~Unit()
{
    for (int i = 0; i < mItems.size(); i++) {
        delete mItems[i];
    }
    mItems.clear();
}

int Unit::getHp()
{
    return mHp;
}

int Unit::getRarityPoints()
{
    return mRarityPoints;
}

int Unit::getCrystals()
{
    return mCrystals;
}

vector<Item*>& Unit::getItems()
{
    return mItems;
}

void Unit::addItem(Item* item)
{
    mItems.push_back(item);
}

void Unit::heal(int amount)
{
    mHp += amount;
}

void Unit::damage(int amount)
{
    mHp -= amount;
    if (mHp < 0) mHp = 0;
}

void Unit::earnCrystal(int amount)
{
    mCrystals += amount;
}

void Unit::spendCrystal()
{
    mCrystals -= 5;
}

void Unit::earnRp(int amount)
{
    mRarityPoints += amount;
}


void Unit::printStats()
{
    cout << "HP: " << mHp << endl;
    cout << "Rarity Points: " << mRarityPoints << endl;
    cout << "Crystals: " << mCrystals << endl;
} 
