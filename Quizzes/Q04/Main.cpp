#include<iostream>
#include<string>
#include<time.h>
#include"Unit.h"
#include"HealPotion.h"
#include"Bomb.h"
#include "Crystal.h"
#include "RareItem.h"
using namespace std;

Unit* createUnit();
void activateItem(Unit* unit, int randomizer, vector<string>& itemName);
void printItems(vector<string>& itemName);
int main() {
	srand(time(NULL));
	Unit* unit = createUnit();
	vector<string> itemName;
	while (unit->getHp() > 0 && unit->getCrystals() > 0 && unit->getRarityPoints() < 100) {
		int randomizer = rand() % 100 + 1;
		unit->spendCrystal();
		unit->printStats();	

		activateItem(unit, randomizer, itemName);
	
		system("pause");
		system("cls");
	}
	if (unit->getRarityPoints() >= 100) {
		cout << "Congrats! You got enough Rarity Points!\n" << endl;

	}
	else {
		cout << "You Lose! Better Luck next time!\n" << endl;
	}
	cout << "Final Stats:" << endl;
	unit->printStats();
	cout << "\nList of Items Pulled:\n" << endl;
	printItems(itemName);
	delete unit;
	return 0;
}
Unit* createUnit()
{
	Unit* unit = new Unit();

	//Sets the Items
	Item* heal = new HealPotion("Health Potion", 30);
	unit->addItem(heal);
	Item* bomb = new Bomb("Bomb", 25);
	unit->addItem(bomb);
	Item* crystal = new Crystal("Crystal", 15);
	unit->addItem(crystal);
	Item* rare = new RareItem("R", 5);
	unit->addItem(rare);
	Item* superRare = new RareItem("SR", 10);
	unit->addItem(superRare);
	Item* superSuperRare = new RareItem("SSR", 50);
	unit->addItem(superSuperRare);

	return unit;
}
void activateItem(Unit* unit, int randomizer, vector<string>& itemName)
{
	if (randomizer == 1) {
		Item* pulledItem = unit->getItems()[5]; //SSR Item
		pulledItem->activate(unit);
		itemName.push_back(pulledItem->getName());
	}
	else if (1 < randomizer && randomizer <= 10) {
		Item* pulledItem = unit->getItems()[4]; //SR Item
		pulledItem->activate(unit);
		itemName.push_back(pulledItem->getName());
	}
	else if (10 < randomizer && randomizer <= 25) {
		Item* pulledItem = unit->getItems()[2]; //Crystal Item
		pulledItem->activate(unit);
		itemName.push_back(pulledItem->getName());
	}
	else if (25 < randomizer && randomizer <= 40) {
		Item* pulledItem = unit->getItems()[0]; //Heath Potion Item
		pulledItem->activate(unit);
		itemName.push_back(pulledItem->getName());
	}
	else if (40 < randomizer && randomizer <= 60) {
		Item* pulledItem = unit->getItems()[1]; //Bomb Item
		pulledItem->activate(unit);
		itemName.push_back(pulledItem->getName());
	}
	else if (60 < randomizer && randomizer <= 100) {
		Item* pulledItem = unit->getItems()[3];	//R Item
		pulledItem->activate(unit);
		itemName.push_back(pulledItem->getName());
	}
	
}
void printItems(vector<string>& itemName)
{
	int potionCount = 0, bombCount = 0, crystalCount = 0, rCount = 0, srCount = 0, ssrCount = 0;
	for (int i = 0; i < itemName.size(); i++) {
		if (itemName[i] == "Health Potion") potionCount++;
		if (itemName[i] == "Bomb") bombCount++;
		if (itemName[i] == "Crystal") crystalCount++;
		if (itemName[i] == "R") rCount++;
		if (itemName[i] == "SR") srCount++;
		if (itemName[i] == "SSR") ssrCount++;
	}
	cout << "Health Potions: " << potionCount << endl;
	cout << "Bombs: " << bombCount << endl;
	cout << "Crystals: " << crystalCount << endl;
	cout << "R Items: " << rCount << endl;
	cout << "SR Items: " << srCount << endl;
	cout << "SSR Items: " << ssrCount << endl;
}
