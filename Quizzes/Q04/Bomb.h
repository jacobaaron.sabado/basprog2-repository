#pragma once
#include<string>
#include<iostream>
#include "Item.h"
using namespace std;
class Bomb :
    public Item
{
public:
    Bomb(string name, int damageValue);

    void activate(Unit*target);

private:
    int mdamageValue;
};

