#pragma once
#include<string>
#include<iostream>
#include<vector>
#include"Item.h"

using namespace std;

class Item;

class Unit
{
public:
	Unit();
	~Unit();
	//Getters
	int getHp();
	int getRarityPoints();
	int getCrystals();
	vector<Item*>& getItems();

	void addItem(Item* item);
	void heal(int amount);
	void damage(int amount);
	void earnCrystal(int amount);
	void spendCrystal();
	void earnRp(int amount);
	void printStats();

private:
	int mHp;
	int mRarityPoints;
	int mCrystals;
	vector<Item*> mItems;
};

