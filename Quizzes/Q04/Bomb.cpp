#include "Bomb.h"

Bomb::Bomb(string name, int damageValue)
	: Item(name)
{
	mdamageValue = damageValue;
}

void Bomb::activate(Unit* target)
{
	Item::activate(target);
	target->damage(mdamageValue);
	cout << "It explodes! You took " << mdamageValue << " damage!" << endl;
}
