#pragma once
#include<string>
#include<iostream>
#include "Unit.h"

using namespace std;

class Unit;

class Item
{
public:
	Item(string name);

	//Getter
	string getName();

	virtual void activate(Unit* target);

private:
	string mName;
};

