#include "Item.h"

Item::Item(string name)
{
	mName = name;
}

string Item::getName()
{
	return mName;
}

void Item::activate(Unit* target)
{
	cout << "You pulled " << mName << "!" << endl;
}
