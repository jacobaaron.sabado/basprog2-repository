#include "HealPotion.h"

HealPotion::HealPotion(string name, int healValue)
	: Item(name)
{
	mHealValue = healValue;
}

void HealPotion::activate(Unit* target)
{
	Item::activate(target);

	target->heal(mHealValue);
	cout << "You are healed for " << mHealValue << " HP!" << endl;
}
