#include "RareItem.h"

RareItem::RareItem(string name, int rarityValue)
	: Item(name)
{
	mRarityValue = rarityValue;
}

void RareItem::activate(Unit* target)
{
	Item::activate(target);
	target->earnRp(mRarityValue);
	cout << "Your Rarity Points increase by " << mRarityValue << "!" << endl;
}
