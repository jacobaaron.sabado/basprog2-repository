#pragma once
#include "Item.h"
class Crystal :
    public Item
{
public:

    Crystal(string name, int gainCrystals);

    void activate(Unit* target);

private:
    int mGainCrystals;
};

