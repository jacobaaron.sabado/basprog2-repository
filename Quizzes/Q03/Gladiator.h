#pragma once
#include <string>
#include <iostream>
using namespace std;

class Gladiator
{
public:
	Gladiator(string name, int selector);
	//Getters
	string getName();
	string getClass();
	int getClassIndex();
	int getHp();
	int getMaxHp();
	int getPow();
	int getVit();
	int getDex();
	int getAgi();

	//Setter for the enemy
	void setName();
	//Setters (for modifying values)
	void setHp(int value);
	void setPow(int value);
	void setVit(int value);
	void setDex(int value);
	void setAgi(int value);

	//Behaviours
	void printStats();
	bool isAlive();
	void takeDamage(int damage);
	void attack(Gladiator* target);
	
private:
	string mName;
	string mClass;
	int mClassIndex;
	int mHp;
	int mMaxHp;
	int mPow;
	int mVit;
	int mDex;
	int mAgi;
};

