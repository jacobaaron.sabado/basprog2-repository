#include <iostream>
#include <string>
#include <time.h>
#include "Gladiator.h"

using namespace std;

void enemyModifier(Gladiator* enemyCharacter, int enemyStat){
	enemyCharacter->setName();
	enemyCharacter->setPow(enemyStat);
	enemyCharacter->setVit(enemyStat);
	enemyCharacter->setDex(enemyStat);
	enemyCharacter->setAgi(enemyStat);
}

void action(Gladiator* actor, Gladiator* target){
	actor->attack(target);
}

void victoryReward(Gladiator* playerCharacter, Gladiator* target) {
	int className = target->getClassIndex();
	switch (className)
	{
	case(1):
		cout << "POW increased to 3!" << endl << "VIT increased to 3!" << endl;
		playerCharacter->setPow(3);
		playerCharacter->setVit(3);
		break;
	case(2):
		cout << "DEX increased to 3!" << endl << "AGI increased to 3!" << endl;
		playerCharacter->setDex(3);
		playerCharacter->setAgi(3);
		break;
	case(3):
		cout << "POW increased to 5!" << endl;
		playerCharacter->setPow(5);
		break;
	default:
		break;
	}
	int hpHealed = playerCharacter->getMaxHp() * .3;
	cout << "You are healed for " << hpHealed << " HP!" << endl;
	playerCharacter->setHp(hpHealed);
}

void playRound(Gladiator* playerCharacter, int enemyStat, int& stage) {
	playerCharacter->printStats();
	int randomizedClass = rand() % 3 + 1;
	Gladiator* enemyCharacter = new Gladiator("Gladiator", randomizedClass);
	enemyModifier(enemyCharacter, enemyStat);
	enemyCharacter->printStats();
	cout << "\nLet the games begin!\n" << endl;
	system("pause");
	system("cls");

	while (playerCharacter->isAlive() && enemyCharacter->isAlive()) {
		if (playerCharacter->getAgi() >= enemyCharacter->getAgi()){
			action(playerCharacter, enemyCharacter);
			if (enemyCharacter->isAlive() == false) {
			    cout << enemyCharacter->getName() << " is dead" << endl;
				system("pause");
				break;
			}
			system("pause");
			action(enemyCharacter, playerCharacter);
			if (playerCharacter->isAlive() == false) {
				cout << playerCharacter->getName() << " is dead" << endl;
				system("pause");
				break;
			} 
			system("pause");
		}
		else {
			action(enemyCharacter, playerCharacter);
			if (playerCharacter->isAlive() == false) {
				cout << playerCharacter->getName() << " is dead" << endl;
				system("pause");
				break;
			}
			system("pause");
			action(playerCharacter, enemyCharacter);
			if (enemyCharacter->isAlive() == false) {
				cout << enemyCharacter->getName() << " is dead" << endl;
				system("pause");
				break;
			}
			system("pause");
		}
	}
	system("cls");
	if (playerCharacter->isAlive()) {
		cout << playerCharacter->getName() << " Wins!" << endl;
		victoryReward(playerCharacter, enemyCharacter);
		delete enemyCharacter;
	}
	else if (enemyCharacter->isAlive()){
		cout << playerCharacter->getName() << " dies! Better luck next time!" << endl;
		return;
	}
	stage++;
	system("pause");
	system("cls");
}

int main(){
	srand(time(NULL));
	int stage = 1;
	string name;
	int classChooser = 0;
	int enemyStat = -3; //To modify after every round

	cout << "What is your name? ";
	cin >> name;
	while (classChooser > 3 || classChooser < 1) {
		cout << "Select your class:\n [1] Warrior\n [2] Assassin\n [3] Mage" << endl;
		cin >> classChooser;
	}
	system("cls");
	Gladiator* playerCharacter = new Gladiator(name, classChooser);

	while (playerCharacter->isAlive()) {
		cout << "ROUND " << stage << endl;
		playRound(playerCharacter, enemyStat, stage);
		enemyStat += 2;
	}

	cout << "You survived " << stage << " rounds.\n\nFinal stats: " << endl;
	playerCharacter->printStats();

	delete playerCharacter;
	return 0;
}