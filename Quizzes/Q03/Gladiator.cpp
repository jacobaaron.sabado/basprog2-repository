#include "Gladiator.h"

Gladiator::Gladiator(string name, int selector)
{
	switch (selector)
	{
	case (1): //Warrior Class
		mName = name;
		mClass = "Warrior";
		mClassIndex = selector;
		mHp = 30;
		mMaxHp = 30;
		mPow = 7;
		mVit = 5;
		mDex = 4;
		mAgi = 4;
		break;
	case (2): //Assassin Class
		mName = name;
		mClass = "Assassin";
		mClassIndex = selector;
		mHp = 27;
		mMaxHp = 27;
		mPow = 7;
		mVit = 3;
		mDex = 8;
		mAgi = 8;
		break;
	case (3): //Mage Class
		mName = name;
		mClass = "Mage";
		mClassIndex = selector;
		mHp = 25;
		mMaxHp = 25;
		mPow = 9;
		mVit = 5;
		mDex = 4;
		mAgi = 4;
		break;

	default:
		break;
	}

}

string Gladiator::getName()
{
	return mName;
}

string Gladiator::getClass()
{
	return mClass;
}

int Gladiator::getClassIndex()
{
	return mClassIndex;
}

int Gladiator::getHp()
{
	return mHp;
}

int Gladiator::getMaxHp()
{
	return mMaxHp;
}

int Gladiator::getPow()
{
	return mPow;
}

int Gladiator::getVit()
{
	return mVit;
}

int Gladiator::getDex()
{
	return mDex;
}

int Gladiator::getAgi()
{
	return mAgi;
}

void Gladiator::setName()
{
	mName = "Enemy " + mClass;
}

void Gladiator::setHp(int value)
{
	mHp += value;
	if (mHp > mMaxHp) mHp = mMaxHp;
}

void Gladiator::setPow(int value)
{
	mPow += value;
}

void Gladiator::setVit(int value)
{
	mVit += value;
}

void Gladiator::setDex(int value)
{
	mDex += value;
}

void Gladiator::setAgi(int value)
{
	mAgi += value;
}


void Gladiator::printStats()
{
	cout << "Name: " << getName() << endl;
	cout << "Class: " << getClass() << endl;
	cout << "HP:" << getHp() << "/" << getMaxHp() << endl;
	cout << "POW:" << getPow() << endl;
	cout << "VIT:" << getVit() << endl;
	cout << "DEX:" << getDex() << endl;
	cout << "AGI:" << getAgi() << endl << endl;
}

bool Gladiator::isAlive()
{
	return mHp > 0;
}

void Gladiator::takeDamage(int damage)
{
	mHp -= damage;
	if (mHp < 0) mHp = 0;
}

void Gladiator::attack(Gladiator* target)
{
	int hitRate = (mDex/target->mAgi) * 100;
	if (hitRate < 20) hitRate = 20;	
	if (hitRate > 80) hitRate = 80;	
	int hitCheck = rand() % 100 + 1;

	if(hitCheck <= hitRate){
	//This is a hit
		int damage = (mPow - target->getVit());
		if (getClassIndex() == 1 && target->getClassIndex() == 2 || getClassIndex() == 2 && target->getClassIndex() == 3 || getClassIndex() == 3 && target->getClassIndex() == 1) damage *= 1.5;
		if (damage < 1) damage = 1;
		cout << getName() << " hits " << target->getName() << " for " << damage << " points!" << endl;
		target->takeDamage(damage);
	}
	else {
	//This is a miss
		cout << getName() << " misses!" << endl;
	}

}



