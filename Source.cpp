#include<iostream>
#include<string>
#include<time.h>

using namespace std;

//Entire code below is the one with no references
//int betMoney(int money, int bet) {
//
//	cout << "How much would you like to bet? You have " << money << " gold." << endl;
//	cin >> bet;
//
//	//if money bet is more than current money or less than 0, cancels function
//	if (bet > money || bet <= 0) {
//		cout << "Invalid bet. Try Again." << endl;
//		system("pause");
//		return;
//	}
//	return money;
//}

void betMoney (int& moneyRef, int& betRef){
	//Asks for bet
	cout << "How much would you like to bet? You have " << moneyRef << " gold." << endl;
	cin >> betRef;

	//if money bet is more than current money or less than 0, cancels function
		if (betRef > moneyRef || betRef <= 0) {
			cout << "Invalid bet. Try Again." << endl;
			system("pause");
			return;
		}

}

void diceRoll(int& diceRoll1Ref, int&diceRoll2Ref) {
	//Rolls 2 dice
	diceRoll1Ref = rand() % 6 + 1;

	diceRoll2Ref = rand() % 6 + 1;

}

void payOut(int& moneyRef, int& betRef, int& aiRollRef, int& playerRollRef){
	//If both have the same value, it's a draw
	if (playerRollRef == aiRollRef) {
		cout << "A draw. No one wins anything." << endl;
		system("pause");
		moneyRef += betRef;
	}
	//If Player's value is more, they win the bet
	if (playerRollRef > aiRollRef) {
		cout << "Congrats. You won the round." << endl;
		system("pause");
		moneyRef += betRef; //Gain money
    }
	//If player rolls Snake Eyes, triple the bet
	if (playerRollRef == 2) {
		cout << "Snake Eyes! Impressive. You win thrice your bet!" << endl;
		system("pause");
		moneyRef += betRef * 3; 

	}
	//Or else if ai rolls higher, player loses
	else if (aiRollRef > playerRollRef) {
		cout << "Tough luck my friend. You lost." << endl;
		system("pause");
		moneyRef -= betRef; //Lose money
	}
	//Prints out final money
	cout << "You now have " << moneyRef << " gold." << endl;
	system("pause");
	system("cls");
}

void playRound(int& playerRollRef, int& aiRollRef, int& diceRoll1Ref, int& diceRoll2Ref, int& moneyRef, int& betRef){

	betMoney(moneyRef, betRef);
	if (betRef > moneyRef || betRef <= 0) {
		return;
	}
	cout << "A.I. rolls the dice..." << endl;
	system("pause");
	diceRoll(diceRoll1Ref, diceRoll2Ref);
	aiRollRef = diceRoll1Ref + diceRoll2Ref;

	cout << "It rolled a " << diceRoll1Ref << " and a " << diceRoll2Ref << ". A total of " << aiRollRef << endl;
	system("pause");

    cout << "You roll the dice..." << endl;
	system("pause");
	diceRoll(diceRoll1Ref, diceRoll2Ref);
	playerRollRef = diceRoll1Ref + diceRoll2Ref;

	cout << "You rolled a " << diceRoll1Ref << " and a " << diceRoll2Ref << ". A total of " << playerRollRef << endl;
	system("pause");

	payOut(moneyRef, betRef, aiRollRef, playerRollRef);
}

int main() {
	srand(time(NULL));
	int aiRoll;
	int playerRoll;
	int diceRoll1;
	int diceRoll2;
	int money = 1000;
	int bet;
	
	while (money != 0) {
		playRound(playerRoll, aiRoll, diceRoll1, diceRoll2, money, bet);   
	}

	cout << "Ha, you ran out of money. Too bad." << endl;
	system("pause");

	return 0;
}